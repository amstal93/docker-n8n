FROM alpine

ARG VERSION

ENV ADDITIONAL_MODULES="ws" \
    TZ="UTC" \
    DATA_FOLDER="/data" \
    UID=1000 \
    GID=1000

WORKDIR /app
RUN set -e && \
    sed -i 's/https\:\/\/dl-cdn.alpinelinux.org/https\:\/\/ewr.edge.kernel.org/g' /etc/apk/repositories && \
    apk add --no-cache --virtual .build-deps git python3 build-base ca-certificates && \
    apk add --no-cache --virtual .run-deps nodejs-current npm graphicsmagick tzdata su-exec && \
    ln -sf /usr/bin/python3 /usr/bin/python && \
    npm config set unsafe-perm true && \
    npm_config_user=root npm install -g full-icu && \
    npm_config_user=root npm install n8n@${VERSION} && \
    npm_config_user=root npm uninstall -g full-icu && \
    npm_config_user=root npm cache clean --force && \
    apk del .build-deps && \
    rm -rf /usr/bin/python -rf /tmp/* /var/cache/apk/*

COPY entrypoint.sh /app/entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
CMD ["/app/node_modules/n8n/bin/n8n"]
