#!/bin/bash

apk add curl jq

[[ ! -f EXISTING ]] || touch EXISTING
EXISTING=$(cat EXISTING)
echo "Existing: ${EXISTING}"

if [[ -n $OVERWRITE ]]; then
  echo "Overwriting: $OVERWRITE"
  LATEST=$OVERWRITE
else
  for i in {1..6}; do # check 6 tags for a suitable
    NAME=$(curl -ks 'https://api.github.com/repos/n8n-io/n8n/git/matching-refs/tags/n8n@' | jq -r ".[-${i}].ref")
    if [[ $NAME == 'refs/tags/n8n@'* ]]; then
      LATEST=${NAME/refs\/tags\/n8n@/}
      echo "Latest: ${LATEST}"
      break
    else
      echo "Ignoring $NAME"
    fi
  done
fi

if [[ (-n "${LATEST}" && "${LATEST}" != "${EXISTING}") ]]; then
  echo "${LATEST}" > LATEST
  echo "Building..."
fi
