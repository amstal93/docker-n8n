#!/bin/ash

set -e
[[ "$DEBUG" == "true" ]] && set -x

cd /app

if [[ -n "${ADDITIONAL_MODULES}" ]]; then
  npm install ${ADDITIONAL_MODULES}
  export NODE_FUNCTION_ALLOW_EXTERNAL=${ADDITIONAL_MODULES},${NODE_FUNCTION_ALLOW_EXTERNAL}
fi

getent group n8n >/dev/null || addgroup -g ${GID} n8n
getent passwd n8n >/dev/null || adduser -h /data -s /bin/sh -G n8n -D -u ${UID} n8n
mkdir -p /data/.n8n /home/node
chmod o+rx /data
chown -R n8n:n8n /data
ln -sf /root/.n8n /home/node/

exec su-exec n8n:n8n "$@"
